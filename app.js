var ball = { x: 0, y: 0 }

function initialization() {
	initTime();
	createThings();
	putThingsInInitialPositions();
	initGameCycle();
}

function createThings() {
}

function initTime() {
}

function initGameCycle() {
}

function putThingsInInitialPositions() {
}

function gameCycle() {
	updateTime();
	checkKeyboard();
	checkStage();
	updateTexts();
	updateWorld();
	drawWorld();
}

function checkKeyboard() {
}

function updateTime() {
}

function drawWorld() {
}

function updateWorld() {
	updateLetters();
	updateBall();
	checkCollisions();
}

updateTime() {
}
	
checkStage() {
}

function updateLetters() {
}

updateBall() {
}

checkCollisions() {
}
	
function drawWorld() {
}


/*
data = context.getImageData(x, y, 1, 1).data;
color = new Color([data[0], data[1], data[2]]);
*/

/*
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
ctx.font = "30px Arial";
ctx.fillText("Hello World",10,50);
*/


